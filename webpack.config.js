const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackAutoInject = require('webpack-auto-inject-version');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const package = require('./package.json');

const PATHS = {
  src: path.join(__dirname, './src'),
  dist: path.join(__dirname, './build'),
  templates: path.join(__dirname, './src/templates'),
};

module.exports = {
  entry: path.join(PATHS.src, 'app.js'),
  output: {
    path: PATHS.dist,
    filename: 'bundle.[chunkhash].js',
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.hbs$/,
        use: 'handlebars-loader',
      },
      {
        test: /\.md$/,
        use: ['html-loader', 'markdown-loader'],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      favicon: path.join(PATHS.src, 'favicon.ico'),
      //template: path.join(PATHS.templates, 'index.hbs'),
      template: path.join(PATHS.templates, 'index.html'),
      title: package.name,
    }),
    new WebpackAutoInject({
      components: {
        AutoIncreaseVersion: true,
      },
    }),
    new CleanWebpackPlugin(PATHS.dist),
  ],
  devServer: {
    port: 3000,
    compress:
            true,
    stats:
            'errors-only',
    open:
            false,
    contentBase:
            path.resolve(__dirname, 'app'),
    publicPath:
            '/',
  },
};
